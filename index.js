// Import thư viện Express Js
const express = require("express");

// Khởi tạo 1 app express
const app = express();

// Khai báo 1 cổng 
const port = 8000;

app.use('/', (req, res, next) => {
    console.log(new Date());

    next();
});

// khai báo middleware
const {
    getAllDrinkMiddleware,
    getDrinkMiddleware,
    postDrinkMiddleware,
    putDrinkMiddleware,
    deleteDrinkMiddleware
} = require('../task-506.30-devcamp-pizza365/app/routes/drinkRouter');

// tạo ra Router drink
const drinkRouter = express.Router();
// Khởi tạo các phương thức middleware drink
drinkRouter.get('/drinks', getAllDrinkMiddleware, (req, res) => {
    res.json({
        message: "Get all drink"
    })
});
// phương thức post
drinkRouter.post('/drinks', postDrinkMiddleware, (req, res) => {
    res.json({
        message: "Create a drink"
    })
});
// phương thức get drink by Id
drinkRouter.get('/drinks/:drinkId', getDrinkMiddleware, (req, res) => {
    let drinkId = req.params.drinkId;
    res.json({
        message: `Get a drink by Id = ${drinkId}`
    })
});
// phương thức put drink by Id
drinkRouter.put('/drinks/:drinkId', putDrinkMiddleware, (req, res) => {
    let drinkId = req.params.drinkId;
    res.json({
        message: `Update a drink by Id = ${drinkId}`
    })
});
// phương thức delete drink by Id
drinkRouter.delete('/drinks/:drinkId', deleteDrinkMiddleware, (req, res) => {
    let drinkId = req.params.drinkId;
    res.json({
        message: `Delete a drink by Id = ${drinkId}`
    })
});
app.use('/', drinkRouter);

// khai báo các middleware voucher
const {
    getAllVoucherMiddleware,
    getVoucherMiddleware,
    postVoucherMiddleware,
    putVoucherMiddleware,
    deleteVoucherMiddleware
} = require('../task-506.40-pizza365-voucher-router/app/routes/voucherRouter');

// tạo ra voucher router
const voucherRouter = express.Router();

// Khởi tạo các phương thức với middle voucher
voucherRouter.get('/vouchers', getAllVoucherMiddleware, (req, res) => {
    res.json({
        message: "Get all voucher"
    })
});
// phương thức post
voucherRouter.post('/vouchers', postVoucherMiddleware, (req, res) => {
    res.json({
        message: "Create a voucher"
    })
});
// phương thức get drink by Id
voucherRouter.get('/vouchers/:voucherId', getVoucherMiddleware, (req, res) => {
    let voucherId = req.params.voucherId;
    res.json({
        message: `Get a voucher by Id = ${voucherId}`
    })
});
// phương thức put drink by Id
voucherRouter.put('/vouchers/:voucherId', putVoucherMiddleware, (req, res) => {
    let voucherId = req.params.voucherId;
    res.json({
        message: `Update a voucher by Id = ${voucherId}`
    })
});
// phương thức delete drink by Id
voucherRouter.delete('/vouchers/:voucherId', deleteVoucherMiddleware, (req, res) => {
    let voucherId = req.params.voucherId;
    res.json({
        message: `Delete a voucher by Id = ${voucherId}`
    })
});
app.use('/', voucherRouter);

// khai báo các middleware user
const {
    getAllUserMiddleware,
    getUserMiddleware,
    postUserMiddleware,
    putUserMiddleware,
    deleteUserMiddleware
} = require('../task-506.50-pizza365-user-router/app/routes/userRouter');
// tạo ra userRouter 
const userRouter = express.Router();

// Khởi tạo các phương thức với middle voucher
userRouter.get('/users', getAllUserMiddleware, (req, res) => {
    res.json({
        message: "Get all users"
    })
});
// phương thức post
userRouter.post('/vouchers', postUserMiddleware, (req, res) => {
    res.json({
        message: "Create a users"
    })
});
// phương thức get drink by Id
userRouter.get('/users/:userId', getUserMiddleware, (req, res) => {
    let userId = req.params.userId;
    res.json({
        message: `Get a users by Id = ${userId}`
    })
});
// phương thức put drink by Id
userRouter.put('/users/:userId', putUserMiddleware, (req, res) => {
    let userId = req.params.userId;
    res.json({
        message: `Update a users by Id = ${userId}`
    })
});
// phương thức delete drink by Id
userRouter.delete('/users/:userId', deleteUserMiddleware, (req, res) => {
    let userId = req.params.userId;
    res.json({
        message: `Delete a users by Id = ${userId}`
    })
});
app.use('/', userRouter);

// run app on declared port
app.listen(port, () => {
    console.log(`App running on port ${port}`);
});